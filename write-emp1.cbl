       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. CHENPOB.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "emp1.dat" 
             ORGANIZATION IS LINE SEQUENTIAL.
             
       DATA DIVISION. 
       FILE SECTION.
       FD  EMP-FILE.
       01  EMP-DETAILS.
           88 END-OF-EMP-FILE  VALUE HIGH-VALUE .
           05 EMP-SSN   PIC  9(9).
           05 EMP-NAME.
              10 EMP-SURNAME PIC X(15).
              10 EMP-FORENAME PIC X(10).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-YOB  PIC 9(4).
              10 EMP-MOB  PIC 9(2).
              10 EMP-DOB  PIC 9(2).
           05 EMP-GENDER PIC X.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT EMP-FILE 
           MOVE "123456789" TO EMP-SSN 
           MOVE "NOPAKIAT" TO  EMP-SURNAME
           MOVE "CHENPOB" TO EMP-FORENAME  
           MOVE "20000820" TO  EMP-DATE-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS 

           MOVE "543216789" TO EMP-SSN 
           MOVE "TISONTI" TO  EMP-SURNAME 
           MOVE "NAPHAT" TO  EMP-FORENAME
           MOVE "20000919" TO  EMP-DATE-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS 

           MOVE "678954321" TO EMP-SSN 
           MOVE "KROMKREW" TO  EMP-SURNAME 
           MOVE "JIRAT" TO  EMP-FORENAME
           MOVE "20001122" TO  EMP-DATE-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS 

           CLOSE  EMP-FILE  
           GOBACK

       .
